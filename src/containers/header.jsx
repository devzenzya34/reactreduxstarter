import React, {Component} from 'react';
import { connect } from 'react-redux'
import * as actions from '../actions'
import { Link } from "react-router-dom";

class Header extends Component {

    onClickAuthentification = () => {
        this.props.setAuthentification(!this.props.isLoggedIn)
    }

    //Gestion du changement de label selon l'état
    renderAuthentificationLabel = () => {
        if(this.props.isLoggedIn) {
            return "Déconnexion"
        } else {
            return "Connexion"
        }
    }

    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                <div className="container">
                    <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                        <div className="navbar-nav">
                            <Link to="/" className="nav-link active">Accueil<span className="sr-only">(current)</span></Link>
                            <Link to="/ressources" className="nav-link">Ressources</Link>
                            <a className="nav-link" href="#" onClick={this.onClickAuthentification}>{this.renderAuthentificationLabel()}</a>
                        </div>
                    </div>
                </div>
            </nav>
        );
    }
}

const mapStateToProps = (state) => {
    return {
    isLoggedIn: state.authentification.isLoggedIn
    }
}

export default connect(mapStateToProps, actions)(Header);